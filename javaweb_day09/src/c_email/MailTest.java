package c_email;

import org.junit.Test;

import javax.mail.MessagingException;

/*
    测试发送邮件
 */
public class MailTest {
    @Test
    public void testSend(){
        /*
            参数1: 发送给谁(邮箱)
            参数2: 发送的内容
            参数3: 标题
         */
        try {
            MailUtils.sendMail(
                    "eoron77@163.com",
                    "java2207测试内容",
                    "这是测试邮件");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}

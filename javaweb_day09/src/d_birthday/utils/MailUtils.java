package d_birthday.utils;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.util.Properties;

public class MailUtils {
	public static void sendMail(String email, String emailMsg,String sub)
			throws AddressException, MessagingException {
		// 1.创建一个程序与邮件服务器会话对象 Session

		//创建属性集
		Properties props = new Properties();
		//protocol: 协议, SMTP:用来发送邮件的协议
		props.setProperty("mail.transport.protocol", "SMTP");
		/* host: 服务器地址
			SMTP: smtp.sina.cn
		 */
		props.setProperty("mail.host", "smtp.sina.cn");
		//auth: 授权,验证 -> true:表示要验证
		props.setProperty("mail.smtp.auth", "true");// 指定验证为true

		// 创建验证器
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				/*	当前客户端的用户名15778067152@sina.cn
					参数1:用户名,就是账号,是不带@部分的内容
					参数2:密码,不是登录密码,是刚设置的授权码
						授权码: 761b7acfcfe9fd73
				 */
				return new PasswordAuthentication
						("15778067152", "761b7acfcfe9fd73");
			}
		};

		Session session = Session.getInstance(props, auth);

		// 2.创建一个Message，它相当于是邮件内容
		Message message = new MimeMessage(session);
		//设置从哪里发送: 给的就是邮箱地址
		message.setFrom(new InternetAddress("15778067152@sina.cn")); // 设置发送者
		//设置发送到什么地方,email参数就是邮箱地址
		message.setRecipient(RecipientType.TO, new InternetAddress(email)); // 设置发送方式与接收者
		//Subject: 发送信息的主题
		//message.setSubject("用户激活");
		message.setSubject(sub);
		// message.setText("这是一封激活邮件，请<a href='#'>点击</a>");
		//emailMsg:邮件的具体内容以什么方式进行解析
		message.setContent(emailMsg, "text/html;charset=utf-8");

		// 3.创建 Transport用于将邮件发送
		Transport.send(message);
	}
}

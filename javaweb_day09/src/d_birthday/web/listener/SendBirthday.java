package d_birthday.web.listener;

import d_birthday.service.CostomerService;
import d_birthday.service.impl.CostomerServiceImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import java.util.Timer;
import java.util.TimerTask;

/*
    定时发送生日祝福
 */
@WebListener
public class SendBirthday implements ServletContextListener {
    //3.创建业务层对象
    private CostomerService cs = new CostomerServiceImpl();

    @Override//开启监听器
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        //1.开启定时发送生日祝福的周期性任务
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //2.发送生日祝福, 发送给谁? 查询数据库
                cs.send();
            }
        },3000,10000);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}

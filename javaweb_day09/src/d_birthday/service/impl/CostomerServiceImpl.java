package d_birthday.service.impl;

import c_email.MailUtils;
import d_birthday.dao.CostomerDao;
import d_birthday.dao.impl.CostomerDaoImpl;
import d_birthday.domain.Customer;
import d_birthday.service.CostomerService;

import javax.mail.MessagingException;
import java.util.List;

public class CostomerServiceImpl implements CostomerService {
    private CostomerDao dao = new CostomerDaoImpl();

    @Override//发送邮件的具体方法
    public void send() {
        //给当天生日的每一个人发送邮件 -> 查询数据库
        List<Customer> list = dao.findCostomerBirthday();

        //遍历集合获取每一个用户
        for(Customer customer : list){
            //发送邮件 - 调用MailUtils
            /*
                参数1: 发送给谁(邮箱)
                参数2: 发送的内容
                参数3: 标题
             */
                try {
                    MailUtils.sendMail(
                            "eoron77@163.com",
                            "亲爱的:" +  customer.getRealname() + "你好,祝你生日快乐!",
                            "生日祝福");
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
        }

    }
}

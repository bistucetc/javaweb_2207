package d_birthday.dao.impl;

import d_birthday.dao.CostomerDao;
import d_birthday.domain.Customer;
import d_birthday.utils.DruidUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CostomerDaoImpl implements CostomerDao {
    private JdbcTemplate template =
            new JdbcTemplate(DruidUtils.getDataSource());

    @Override
    public List<Customer> findCostomerBirthday() {
        String sql = "SELECT * FROM customer WHERE MONTH(birthday) = MONTH(NOW()) AND DAY(birthday) = DAY(NOW());";
        return template.query
                (sql,
                new BeanPropertyRowMapper<Customer>(Customer.class));
    }





}

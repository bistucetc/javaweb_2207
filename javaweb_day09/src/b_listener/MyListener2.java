package b_listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Timer;
import java.util.TimerTask;

/*
    监听器的使用 - 注解配置
 */
@WebListener
public class MyListener2 implements ServletContextListener {

    /*监听servlet对象创建的ServletContext对象 - 服务器开启时
        一般会在这里进行加载资源或者开启周期性任务
     */
    public void contextInitialized(ServletContextEvent sce) {
      //开启周期性任务
        Timer timer = new Timer();
        //创建行程表
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //定时且重复的事情 -> 发送生日祝福,银行计息
            }
        },1000,1000);


    }

    @Override//在服务器关闭后,ServletContext对象被销毁
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("ServletContext对象被销毁了...");
    }
}





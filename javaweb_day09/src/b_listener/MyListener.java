package b_listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/*
    监听器的使用 - web.xml配置方式
 */
public class MyListener implements ServletContextListener {

    /*监听servlet对象创建的ServletContext对象 - 服务器开启时
        一般会在这里进行加载资源或者开启周期性任务
     */
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("ServletContext对象被创建了...");
        //功能实现: 加载资源文件
        //1.获取servletContext对象
        ServletContext sc = sce.getServletContext();
        //2.进行配置文件的加载 - 在调用之前需要去web.xml配置参数
        String location = sc.getInitParameter("location");
        //3.获取真实路径
        ///WEB-INF/classes/applicationLocationContext.xml
        String realPath = sc.getRealPath(location);
        //4.将获取到的文件加载到内存中 - 流进行加载
        try {
            FileInputStream fis = new FileInputStream(realPath);
            System.out.println(fis);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }


    }

    @Override//在服务器关闭后,ServletContext对象被销毁
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("ServletContext对象被销毁了...");
    }
}





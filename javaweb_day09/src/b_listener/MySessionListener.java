package b_listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/*
    监听域中内容的监听器
 */
@WebListener
public class MySessionListener implements HttpSessionAttributeListener {

    /* 添加方法 */
    public void attributeAdded(HttpSessionBindingEvent se) {
        //获取添加进入的元素
        String name = se.getName();//获取域中存储的键名
        Object value = se.getValue();//获取域中存储的value值
        System.out.println("添加元素--> " + name + ": " + value);
    }

    /* 删除方法 */
    public void attributeRemoved(HttpSessionBindingEvent se) {
        //获取被删除的元素
        String name = se.getName();//获取域中存储的键名
        Object value = se.getValue();//获取域中存储的value值
        System.out.println("删除元素--> " + name + ": " + value);
    }

    /* 替换方法 */
    public void attributeReplaced(HttpSessionBindingEvent se) {
        //获取被替换的元素
        String name = se.getName();//获取域中存储的键名
        Object value = se.getValue();//获取域中存储的value值
        System.out.println("替换元素--> " + name + ": " + value);
    }
}







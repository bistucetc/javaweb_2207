package web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/*
    过滤器(拦截器)
 */
//拦截所有资源,表示访问所有资源之前都会执行该过滤方法(doFilter)
//@WebFilter("/*")
//拦截单个资源,表示只有访问这个jsp界面的时候,资源才会被拦截下来
@WebFilter("/hello.jsp")
public class AFilterDemo implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override//核心方法 - 过滤方法
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        //放行前,对request资源进行处理
        System.out.println("2.FilterDemo2放行前....");

        //放行 filterChain
        chain.doFilter(request,response);

        //放行后,对response响应资源进行处理
        System.out.println("4.FilterDemo2放行后....");
    }
    @Override
    public void destroy() {

    }
}

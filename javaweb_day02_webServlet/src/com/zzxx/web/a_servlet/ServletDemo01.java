package com.zzxx.web.a_servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;

/*@WebServlet: 标记servlet的注解
    完全匹配
 */
//@WebServlet(value = "/ServletDemo01")
//@WebServlet("/ServletDemo01")
//多路径匹配
@WebServlet(urlPatterns = {"/demo01","/web01","/servlet01"})
public class ServletDemo01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write(new Date().toString());
    }
}
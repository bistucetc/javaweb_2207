package a_response;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    重定向的实现
 */
@WebServlet("/ResponseDemo01")
public class ResponseDemo01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.设置响应行信息 - 状态码设置为重定向
        response.setStatus(302);
        //2.设置响应头的信息 - 重定向地址
        response.setHeader("location","http://www.baidu.com");
        //设置到自定义的资源地址(/javaweb_day03/Servlet)

        //重定向之后,不携带到另一个Servlet资源中

    }
}
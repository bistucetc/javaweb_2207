package a_response;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/*
    响应体: 输出流输出内容
 */
@WebServlet("/ResponseDemo04")
public class ResponseDemo04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置response缓冲区的字符集
        //response.setCharacterEncoding("UTF-8");

        //3.设置浏览器的响应类型,解析方式
        response.setContentType("text/html;charset=utf-8");

        //1.获取字符流  -> 输出内容(页面)
        PrintWriter writer = response.getWriter();

       /*2.写入内容
            response缓冲区的编码集: ISO8859-1
        */
        writer.println("<h1>字符流输出println方法</h1>");
        writer.write("<h1>字符流输出write方法</h1>");

    }
}
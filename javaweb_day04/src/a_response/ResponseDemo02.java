package a_response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
    重定向的实现 - 优化方式
 */
@WebServlet("/ResponseDemo02")
public class ResponseDemo02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.动态获取项目的虚拟目录
        String contextPath = request.getContextPath();
        System.out.println(contextPath);
        //2.重定向到自定义的资源地址 -> /javaweb_day04/ResponseDemo01
        response.sendRedirect(contextPath + "/ResponseDemo01");
    }
}
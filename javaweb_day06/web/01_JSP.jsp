<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP脚本</title>
</head>
<body>
<%--
    1)<% 代码 %>: 在service()
    2)<%! 代码 %>: 在jsp转换成java类中成员位置出现.
    3)<%= 代码 %>: 会输出在页面上.
--%>

    <%--使用html标签--%>
    <h1>Hello,JSP!!!</h1>

    <%-- 1)<% 代码 %>: 在service() --%>
    <%
        //局部变量
        int a = 1;
        if(a == 1){
            System.out.println("a:" + a);
        }else {
            System.out.println("没有输出");
        }
    %>

    <%--2)<%! 代码 %>: 在jsp转换成java类中成员位置出现.--%>
    <%!
        //成员变量
        int a = 10;

        /*if(a == 10){

        }*/
    %>

    <%-- 3)<%= 代码 %>: 会输出在页面上. --%>
    <%= a %>
    <%= 20 %>

</body>
</html>

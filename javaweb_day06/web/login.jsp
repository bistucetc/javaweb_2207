<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录验证</title>

    <!--为了做:点击图片自动切换的效果
          需要引入js库,就需要有一个js的jar包
          存放在: web下面
      -->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script>
        $(function(){
            $("#checkCode").click(function() {
                //alert("测试");
                //方式一:
                // $("#checkCode").prop("src", "/javaee_day14/CheckImgServlet")
                //方式二: this是js对象,可以直接使用
                //this.src = "/javaweb_day05/CheckImgServlet?age=1";
                this.src = "${pageContext.request.contextPath}/CheckImgServlet?random=" + Math.random();
            });
        });
    </script>
    <%--设置错误信息的字体颜色--%>
    <style>
        div{
            color: red;
        }
    </style>
</head>
<body>
<%-- 使用EL表达式动态获取虚拟目录 --%>
<form action="${pageContext.request.contextPath}/LoginServlet" method="post">
<%--<form action="/javaweb_day06/LoginServlet" method="post">--%>
    <table>
        <tr>
            <td>用户名</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>密码</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td>验证码</td>
            <td><input type="text" name="checkCode"></td>
        </tr>
        <tr>
            <td colspan="2"><img id="checkCode" src="${pageContext.request.contextPath}/CheckImgServlet"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="登录"></td>
        </tr>
    </table>
</form>
    <%-- 登录失败显示的信息 --%>
    <div>
        <%= request.getAttribute("cc_error") == null ?
                " " : request.getAttribute("cc_error")
        %>
    </div>
    <div>
        <%= request.getAttribute("login_error") == null ?
            " " : request.getAttribute("login_error")
        %>
    </div>

</body>
</html>

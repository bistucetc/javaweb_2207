<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>pageContext对象</title>
</head>
<body>
    <%-- 四大域对象: 可以通过pageContext对象来获取
        ①.pageContext(page 域对象)
        ②.request(request 域对象)
        ③.session(session 域对象)
        ④.application(application 域对象)
    --%>

    <%
        //作用一: 作为域对象使用
        //往pageContext域对象中存入数据: 域范围: 当前页面
        pageContext.setAttribute("name","zhangsan");

        //取出pageContext域对象的数据
        String name = (String)pageContext.getAttribute("name");
        System.out.println(name);

        //作用二: 通过pageContext 获取其他内置对象
        HttpSession session1 = pageContext.getSession();

        //作用三: 通过pageContext,向其他域对象中存储数据

        //存储到ServletContext域中
        pageContext.setAttribute("name","lucy"
                ,PageContext.APPLICATION_SCOPE);

        //存储到Session域中
        pageContext.setAttribute("name","tom",
                PageContext.SESSION_SCOPE);

        //存储到request域中
        pageContext.setAttribute("name","marry",
                PageContext.REQUEST_SCOPE);

        //存储到自己的域中
        pageContext.setAttribute("name","rose",
                PageContext.PAGE_SCOPE);


    %>


</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>jsp的九大内置对象</title>
</head>
<body>


    <%--①.request:获取用户请求信息--%>
    <%
        //request.getRequestDispatcher("").forward(request,response);
        //虚拟路径
        //String contextPath = request.getContextPath();
    %>

    <%--②.response:获取客户端的响应信息,并处理--%>
    <%
        response.getWriter().write("response...");
        //重定向
        //response.sendRedirect("");
    %>

    <%--③.out:输出内容到界面上--%>
    <%
        out.print("out输出");
    %>


</body>
</html>

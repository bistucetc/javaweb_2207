<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSTL之if标签</title>
</head>
<body>

    <%-- if标签的使用
        if(条件){}
        if(条件)else{}

        test="": 必须存在的属性,接收boolean类型的值
        在c:if标签中,没有else的情况,如果想要else则在定义一个c:if标签
     --%>
    <c:if test="true">
        <%--可以嵌套html代码--%>
        <h1>JSTL标签库1</h1>
    </c:if>
    <%--表示else的情况--%>
    <c:if test="false">
        <%--可以嵌套html代码--%>
        <h1>JSTL标签库2</h1>
    </c:if>


    <%--需求：判断request域中的一个集合是否为空
        如果不为空,则遍历集合
    --%>

    <%--向域中存储数据--%>
    <%
        ArrayList list = new ArrayList();
        list.add("aaa");
        list.add("bbb");
        request.setAttribute("list",list);
        //存入数据
        request.setAttribute("number",4);
    %>

    <%-- test中条件,使用EL表达式 --%>
    <c:if test="${not empty list}">
        遍历集合....
    </c:if>

    <%--需求: 判断一个数字是奇数还是偶数 (向域对象中存入数据)--%>
    <c:if test="${number % 2 != 0}">
        ${number}是奇数
    </c:if>
    <c:if test="${number % 2 == 0}">
        ${number}是偶数
    </c:if>

    <%-- switch/case--%>
    <c:choose>

    </c:choose>
</body>
</html>

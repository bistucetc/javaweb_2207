<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSTL标签库之forEach标签</title>
</head>
<body>
    <%--java中遍历集合 / 数组的方式:
        方式一: for(int i = 0; i < 10; i++){}
          属性:
            begin: 开始值 ==> int i = 0
            end: 结束值 ==> i < 10
            var: 临时变量 ==> int i
            step: 步长 ==> i++
            varStatus: 循环状态对象
                index: 表示容器中元素的下标位置,下标从0开始
                count: 表示循环的次数, 从1开始

        方式二: for(String str : list){}
          属性:
            items: 要遍历的集合或者数组(容器) ==> list
            var: 临时变量 ==> String str
            varStatus: 循环状态对象
                index: 表示容器中元素的下标位置,下标从0开始
                count: 表示循环的次数, 从1开始


    --%>
    <%-- 方式一的实现 --%>
    <c:forEach begin="1" end="10" var="i" step="2">
        ${i}
    </c:forEach>

    <hr>

    <%
        ArrayList list = new ArrayList();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        request.setAttribute("list",list);
    %>

    <%-- 方式二的实现--%>
    <c:forEach items="${list}" var="str" varStatus="s">
        ${s.index}<br>
        ${s.count}<br>
        ${str}<br>
    </c:forEach>

</body>
</html>

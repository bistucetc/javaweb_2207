<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>去除域对象中的数据</title>
</head>
<body>
    <%
        //存储到ServletContext域中
        pageContext.setAttribute("name","lucy",PageContext.APPLICATION_SCOPE);
        //存储到Session域中
        pageContext.setAttribute("name","tom", PageContext.SESSION_SCOPE);
        //存储到request域中
        pageContext.setAttribute("name","marry", PageContext.REQUEST_SCOPE);
        //存储到自己的域中
        pageContext.setAttribute("name","rose", PageContext.PAGE_SCOPE);
    %>

    <%=
        //获取当前页面中的数据
        "application === " +
                pageContext.getAttribute("name",PageContext.APPLICATION_SCOPE)
    %>
    <%=
        "session === " +
                pageContext.getAttribute("name",PageContext.SESSION_SCOPE)
    %>
    <%=
        "request === " +
                pageContext.getAttribute("name",PageContext.REQUEST_SCOPE)
    %>
    <%=
        "page === " +
                pageContext.getAttribute("name",PageContext.PAGE_SCOPE)
    %>

    <%--转发 为了验证--request 取出值--%>
    <%
        request.getRequestDispatcher("/05_JSP.jsp")
                .forward(request,response);
    %>




</body>
</html>

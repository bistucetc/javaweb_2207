<%@ page import="java.util.ArrayList" %>
<%@ page import="domain.User" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EL表达式获取List集合值</title>
</head>
<body>
    <%--存储数据--%>
    <%
        ArrayList list = new ArrayList();
        list.add("aaa");
        list.add("bbb");

        //创建user对象
        User user = new User();
        user.setName("张三");
        user.setAge(18);
        user.setBirthday(new Date());

        list.add(user);


        //将集合存入域对象中
        request.setAttribute("list",list);
    %>

    <%--获取域中的list集合中的数据--%>
    ${list[0]}<br>
    ${list[1]}<br>
    ${list[2]}<br>
    <%--获取集合中,自定义对象中的name属性--%>
    ${list[2].name}

</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 17:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>el表达式获取方式二</title>
</head>
<body>

<%--使用原始方式往域对象中存储数据--%>
<%
    request.setAttribute("name","张三");
    session.setAttribute("name","18");
%>

<%--获取域对象中的数据
    ${键名} 获取值 -> 遵循 从小到大进行获取

    用法等同于 findAttribute(String name)
--%>
${name}

</body>
</html>

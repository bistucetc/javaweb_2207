<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EL表达式获取值的方式</title>
</head>
<body>

<%-- 四大域对象: 可以通过pageContext对象来获取
    ①.pageContext(page 域对象)
    ②.request(request 域对象)
    ③.session(session 域对象)
    ④.application(application 域对象)
--%>
    <%--使用原始方式往域对象中存储数据--%>
    <%
        request.setAttribute("name","张三");
        session.setAttribute("age","18");
    %>

    <%-- EL表达式的语法: ${表达式} --%>
    <%--获取后去域对象中的内容--%>
    ${requestScope.name}
    ${sessionScope.age}

</body>
</html>

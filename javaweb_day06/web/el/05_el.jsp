<%@ page import="java.util.HashMap" %>
<%@ page import="domain.User" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EL表达式获取Map集合中的值</title>
</head>
<body>
    <%--存储数据--%>
    <%
        //创建map集合
        HashMap map = new HashMap();
        map.put("sname","李四");
        map.put("sage","18");

        //创建user对象
        User user = new User();
        user.setName("张三");
        user.setAge(18);
        user.setBirthday(new Date());

        map.put("user",user);

        //将map集合存入到域对象中
        request.setAttribute("map",map);
    %>

    <%--获取域中的数据--%>
    <%--方式一: ${键名.key名称}--%>
    ${map.sage}<br>
    <%-- 方式二: ${键名["key名称"]}--%>
    ${map["sname"]}<br>

    <%--获取集合中user对象中的name--%>
    ${map.user.name}<br>
    ${map.user["name"]}
</body>
</html>

<%@ page import="domain.User" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 18:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EL获取对象值</title>
</head>
<body>
    <%-- 插入数据 --%>
    <%
        //创建user对象
        User user = new User();
        user.setName("张三");
        user.setAge(18);
        user.setBirthday(new Date());

        //将数据存储到request域对象中
        request.setAttribute("u",user);
    %>

    <%--获取域对象中的值 -
        根据对象中的数据获取值 -> getName()
        get去掉 -> Name首字母变小写 -> name
    --%>
    ${requestScope.u.name}
    ${u.age}
    ${u.birthday}


</body>
</html>

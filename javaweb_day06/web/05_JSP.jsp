<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 2022/9/6
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>取出域对象</title>
</head>
<body>
<%--<%=
//获取当前页面中的数据
        "application === " +
                pageContext.getAttribute("name",PageContext.APPLICATION_SCOPE)
%>
<%=
"session === " +
        pageContext.getAttribute("name",PageContext.SESSION_SCOPE)
%>
<%=
"request === " +
        pageContext.getAttribute("name",PageContext.REQUEST_SCOPE)
%>
<%=
"page === " +
        pageContext.getAttribute("name",PageContext.PAGE_SCOPE)
%>--%>

<%--简化方式: 从小到大进行获取,如果当前域对象中的值为null,就往上一层的域获取 --%>
<%=
    pageContext.findAttribute("name")
%>
</body>
</html>

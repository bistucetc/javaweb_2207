package a_checkcode;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    验证码校验
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.设置request编码 - 防止post提交方式出现乱码
        request.setCharacterEncoding("utf-8");
        //2.获取请求参数(包含: username,password,checkCode)
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String checkCode = request.getParameter("checkCode");
        //3.先获取程序(CheckImgServlet)生成的验证码
        HttpSession session = request.getSession();
        String checkcode_session = (String)session.getAttribute("checkcode_session");
        //4.删除 已经使用过的session对象
        session.removeAttribute("checkcode_session");
        //3.1.判断用户输入的验证和程序生成的验证码是否一致
        //5.删除checkcode_session 如果出现空指针异常,则进行判断即可
        if (checkcode_session !=null && checkCode.equals(checkcode_session)){
            //3.2验证码一致 -> 比较用户名和密码是和否一致
            //3.5 测试数据 zhangsan 和 12345
            if ("zhangsan".equals(username) && "12345".equals(password)){
                //3.5-1如果一致,则登录成功
                //3.5-1-1 存储用户信息 -> 需要在success.jsp界面中取出
                session.setAttribute("user",username);
                //2.5-1-2 重定向到新的页面 success.jsp
                response.sendRedirect
                        (request.getContextPath() + "/success.jsp");
            }else {
                //3.5-2如果不一致,则登录失败
                //3.5-2-1 存储提示信息 -> 需要在login.jsp界面中取出
                request.setAttribute("login_error","用户名或密码错误,请重新输入!!");
                //2.5-2-2 转发到当前页面进行展示 -> login.jsp
                request.getRequestDispatcher("/login.jsp").forward(request,response);
            }
        }else {
            //3.3 验证码不一致
            //3.3-1 不正确,需要存储提示信息到session域中
            request.setAttribute("cc_error","验证码错误,请重新输入!!");
            //3.3-2 转发到登录页面
            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }
    }
}







package web.servlet;

import javax.servlet.*;
import java.io.IOException;
import java.util.Date;

/*
    实现Servlet

    Servlet的生命周期:
        1.init -> 初始化
        2.service -> 核心方法
        3.destroy -> 销毁
 */
public class ServletDemo implements Servlet {
    /*
        初始化方法 - 默认创建servlet对象,就进行初始化操作
        只执行一次操作
        修改: 服务器启动就执行该方法
     */
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init...");
    }


    /*
        核心方法: 提供服务的方法
        在发送请求后执行,每发送一次请求,就会执行一次服务方法
     */
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("访问网页成功!!!");
        //响应: servletResponse
        servletResponse.getWriter().println(new Date());
    }

    /*
        销毁方法
        在服务器正常关闭时,执行,只执行一次.
     */
    @Override
    public void destroy() {
        System.out.println("destroy.....");
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

}

package a_json;



import com.fasterxml.jackson.databind.ObjectMapper;
import domain.User;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

/*
    JSON 与 Java对象之间的转换
 */
public class ObjectMapperTest {
    //2.新建核心对象ObjectMapper
    private ObjectMapper mapper = new ObjectMapper();

    /*
    1.User对象转json, json转User对象
        json字符串 = {"name":"张三","age":23}
        user对象 = User{name='张三', age=23}
     */
    @Test
    public void test01() throws IOException {
        //1.新建User对象
        User user = new User("张三",23);
        //User对象转json
        String json = mapper.writeValueAsString(user);
        System.out.println("Json格式: " + json);

        //json转User对象
        User user1 = mapper.readValue(json, User.class);
        System.out.println("java对象:" + user1);

    }

    /*
    2.map<String,String>转json,json转map<String,String>
      json字符串 = {"姓名":"张三","性别":"男"}
      map对象 = {姓名=张三, 性别=男}
     */
    @Test
    public void test02() throws IOException {
        //map<String,String>转json
        HashMap<String,String> map = new HashMap<>();
        map.put("姓名","张三");
        map.put("性别","男");
        String json = mapper.writeValueAsString(map);
        System.out.println("Json格式: " + json);

        //json转map<String,String>
        HashMap hashMap = mapper.readValue(json, HashMap.class);
        System.out.println("java对象:" + hashMap);
    }

    /*
        3.map<String,User>转json, json转map<String,User>
         json字符串 = {
                "Java2206":{"name":"张三","age":23},
                "Java2207":{"name":"李四","age":24}}
        map对象 =
            {
                Java2206=User{name='张三', age=23},
                Java2207=User{name='李四', age=24}
            }
     */
    @Test
    public void test03() throws IOException {
        //map<String,User>转json
        HashMap<String,User> map = new HashMap<>();
        map.put("Java2206",new User("张三",19));
        map.put("Java2207",new User("李四",19));
        String s = mapper.writeValueAsString(map);
        System.out.println("Json格式: " + s);

        //json转map<String,User>
        HashMap hashMap = mapper.readValue(s, HashMap.class);
        System.out.println("java对象:" + hashMap);
    }
}










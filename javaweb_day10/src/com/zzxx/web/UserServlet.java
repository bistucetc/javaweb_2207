package com.zzxx.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    注册页面的校验
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.解决(请求或响应的编码)
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");

        //让服务器处理请求 需要10秒
        /*try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }*/

        //2.获取请求参数
        String username = request.getParameter("username");

        //3.判断用户名是否注册过
        if ("zhangsan".equals(username)){
            response.getWriter().write("<font color='red'>用户名已注册</font>");
        }else {
            response.getWriter().write("<font color='green'>用户名可用</font>");
        }
    }
}
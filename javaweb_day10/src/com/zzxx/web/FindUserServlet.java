package com.zzxx.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.HashMap;

/*
    注册页面
 */
@WebServlet("/FindUserServlet")
public class FindUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");

        //1.接收请求参数 - 用户名
        String username = request.getParameter("username");

        /*2.调用service业务层的逻辑判断....
            方式一: 单个参数封装成对象
            方式二: 多个参数封装成map集合
             json字符串 = {"姓名":"张三","性别":"男"}
         */
        //2.1创建核心对象
        ObjectMapper mapper = new ObjectMapper();
        HashMap<String,Object> map = new HashMap<>();
        //2.添加集合内的参数
        if("lucy".equals(username)){
            //用户判断用户是否存在 - 表示存在
            map.put("userExist",true);
            map.put("msg","用户已存在");
        }else {
            //用户判断用户是否存在 - 表示不存在
            map.put("userExist",false);
            map.put("msg","尚未创建");
        }
        //3.将map集合转换成json
        mapper.writeValue(response.getWriter(),map);
    }
}








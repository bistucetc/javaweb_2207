package dao.impl;

import dao.UserDao;
import domian.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.DruidUtils;

/*
    实现类
 */
public class UserDaoImpl implements UserDao {
    //1.获取连接
    //通过连接池的方式 连接数据库
    private JdbcTemplate template
            = new JdbcTemplate(DruidUtils.getDataSource());

    /* 增加用户数据 */
    @Override
    public void insertUser(User user) {
        //1.编写sql
        String sql = "insert into user values(null,?,?,?,?,?,?)";
        //2.执行sql
        template.update(sql,user.getUsername(),user.getPassword(),
                user.getEmail(),user.getRealname(),user.getGender(),
                user.getBirthday());
    }

    //登录方法
    public User login(User user) {
        //1.编写sql
        String sql =
                "select * from user where username = ? and password = ?";
        /*
            Ctrl + Alt + T:快速生成包裹代码块的方式
         */

        //3.判断是否查询到用户
        //2.执行sql
        try {
            User loginUser = template.queryForObject(
                    sql,
                    new BeanPropertyRowMapper<User>(User.class),
                    user.getUsername(), user.getPassword());
            //查询到用户,返回用户
            return loginUser;
        } catch (DataAccessException e) {
            //没有查询到用户
            return null;
        }


    }
}

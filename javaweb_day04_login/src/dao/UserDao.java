package dao;

import domian.User;

/*
    操作user表的增删改查
 */
public interface UserDao {
    /* 增加用户数据 */
    void insertUser(User user);

    //登录方法 - 根据用户名和密码进行查询
    public User login(User user);
}

package servlet;

import dao.UserDao;
import dao.impl.UserDaoImpl;
import domian.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;

/*
    登录功能
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.设置编码
        request.setCharacterEncoding("UTF-8");
        //2.获取请求参数
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        //3.封装对象
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        //4.调用dao中的login(),获取user对象 -> null / user
        UserDao dao = new UserDaoImpl();
        User loginUser = dao.login(user);

        //5.判断用户(user)是否存在
        if (loginUser == null){
            //5.1 跳转到失败的FailServlet -> 转发
            request.getRequestDispatcher("/FailServlet")
                    .forward(request,response);
        }else {
            //5.2 跳转到成功的SuccessServlet -> 重定向
            //动态获取虚拟目录: req.getContextPath()
            response.sendRedirect(request.getContextPath() + "/SuccessServlet");
        }
    }
}
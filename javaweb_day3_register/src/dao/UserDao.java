package dao;

import domain.User;

/*
    操作user表的到dao
 */
public interface UserDao {

    /* 增加用户数据 */
    void insertUser(User user);

}

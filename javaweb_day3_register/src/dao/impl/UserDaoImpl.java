package dao.impl;

import dao.UserDao;
import domain.User;
import org.springframework.jdbc.core.JdbcTemplate;
import util.DruidUtils;

/*
    userDao 的具体实现类
 */
public class UserDaoImpl implements UserDao {

    //通过连接池的方式 连接数据库
    private JdbcTemplate template
            = new JdbcTemplate(DruidUtils.getDataSource());

    /* 增加用户数据 */
    @Override
    public void insertUser(User user) {
        //1.编写sql
        String sql = "insert into user values(null,?,?,?,?,?,?)";
        //2.执行sql
        template.update(sql,user.getUsername(),user.getPassword(),
                user.getEmail(),user.getRealname(),user.getGender(),
                user.getBirthday());
    }
}

package servlet;


import dao.UserDao;
import dao.impl.UserDaoImpl;
import domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
    regist.html 跳转的 Servlet资源
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.设置编码集
        request.setCharacterEncoding("UTF-8");
        //2.获取请求参数
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String realname = request.getParameter("realname");
        String gender = request.getParameter("gender");
        String birthday = request.getParameter("birthday");


        //3.将获取到的请求参数 封装到User对象中，方便获取
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setRealname(realname);
        user.setGender(gender);
        user.setBirthday(birthday);
        System.out.println(user);

        //4.调用dao中的方法 - 将获取到的User对象信息传递到dao中的方法
        UserDao dao = new UserDaoImpl();
        //4.1 向user表中插入数据
        dao.insertUser(user);


    }
}
package servlet;


import dao.UserDao;
import dao.impl.UserDaoImpl;
import domain.User;
import org.apache.commons.beanutils.BeanUtils;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/*
    regist.html 跳转的 Servlet资源
 */
@WebServlet("/RegisterServlet2")
public class RegisterServlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.设置编码集
        request.setCharacterEncoding("UTF-8");
        //2.获取请求参数 - map
        Map<String, String[]> map = request.getParameterMap();


        //3.将获取到的请求参数 封装到User对象中，方便获取
        User user = new User();
        /*3.1使用BeanUtils封装JavaBean数据
            参数1:需要封装的user对象
            参数2:获取的请求  Map<String, String[]>
                需要的是一个map集合,也就是请求参数返回的map集合
         */
        try {
            BeanUtils.populate(user,map);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        System.out.println(user);

        //4.调用dao中的方法 - 将获取到的User对象信息传递到dao中的方法
        UserDao dao = new UserDaoImpl();
        //4.1 向user表中插入数据
        dao.insertUser(user);


    }
}
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"
         pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 网页使用的语言 -->
<html lang="zh-CN">
<head>
    <!-- 指定字符集 -->
    <meta charset="utf-8">
    <!-- 使用Edge最新的浏览器的渲染方式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- viewport视口：网页可以根据设置的宽度自动进行适配，在浏览器的内部虚拟一个容器，容器的宽度与设备的宽度相同。
    width: 默认宽度与设备的宽度相同
    initial-scale: 初始的缩放比，为1:1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>用户信息管理系统</title>

    <!-- 1. 导入CSS的全局样式 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <script src="js/jquery-2.1.0.min.js"></script>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="js/bootstrap.min.js"></script>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
    <%--添加js代码: 删除前弹出提示框--%>
    <script>
        //根据id进行删除数据
        function deleteUser(id){
            //添加弹窗
            if (confirm("您确定要删除吗?")){
                //跳转到DelUserServlet中显示
                location.href =
                    "${pageContext.request.contextPath
                    }/DelUserServlet?id=" + id
            }
        }

        //给按钮添加绑定事件
        //页面加载函数
        $(function (){
            //给删除按钮添加单击事件
            $("#delSelected").click(function (){
                if (confirm("确认删除?")){
                    //提交form表单
                    $("#form").submit();
                }
            });
            //全选全不选
            $("#first").click(function (){
                //1.确定first的选中状态
                var checkedFlag = $(this).prop("checked");
                //2.让所有的checkBox的选中状态和checkedFlag一致
                $("input").prop("checked",checkedFlag);
            });
        });
    </script>
</head>
<body>
<div class="container">
    <h3 style="text-align: center">用户信息列表</h3>
    <div style="float: left">
        <form class="form-inline" action="${pageContext.request.contextPath}/FindUserByPageServlet" method="post">
            <div class="form-group">
                <label for="exampleInputName2">姓名</label>
                <input type="text" name="name" class="form-control" id="exampleInputName2" >
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">籍贯</label>
                <input type="text" name="address" class="form-control" id="exampleInputEmail2" >
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">邮箱</label>
                <input type="text" name="email" class="form-control" id="exampleInputEmail3" >
            </div>
            <button type="submit" class="btn btn-default">查询</button>
        </form>
    </div>
    <div style="float: right">
        <tr>
            <td colspan="8" align="center">
                <a class="btn btn-primary"
                   href="${pageContext.request.contextPath}/add.jsp">添加联系人</a></td>
        </tr>
        <tr>
            <td colspan="8" align="center">
                <a class="btn btn-primary"
                   href="javascript:void(0)" id="delSelected">删除选中</a></td>
        </tr>
    </div>
    <%--添加form表单标签, 和 删除按钮做绑定--%>
    <form id="form" action="${pageContext.request.contextPath}/DelSelectServlet" method="post">
    <table border="1" class="table table-bordered table-hover">
        <tr class="success">
            <%--选中按钮--%>
            <th><input type="checkbox" id="first" name="first"/> </th>
            <th>编号</th>
            <th>姓名</th>
            <th>性别</th>
            <th>年龄</th>
            <th>籍贯</th>
            <th>QQ</th>
            <th>邮箱</th>
            <th>操作</th>
        </tr>
        <%--遍历list
            items: 是UserListServlet中存储的list集合
            var: 集合中的每个元素, 都是一个user对象
        --%>
        <c:forEach items="${pb.list}" var="user" varStatus="i">
        <tr>
            <%--被绑定的按钮--%>
            <td><input type="checkbox" name="uid" value="${user.id}"/> </td>
            <td>${i.count}</td>
            <td>${user.name}</td>
            <td>${user.gender}</td>
            <td>${user.age}</td>
            <td>${user.address}</td>
            <td>${user.qq}</td>
            <td>${user.email}</td>
            <td><a class="btn btn-default btn-sm"
                   href="${pageContext.request.contextPath}/FindUserServlet?id=${user.id}">修改</a>
                &nbsp;<a class="btn btn-default btn-sm"
                         href="javascript:deleteUser(${user.id})">删除</a></td>
                         <%--href="${pageContext.request.contextPath}/DelUserServlet?id=${user.id}">删除</a></td>--%>
        </tr>
        </c:forEach>
    </table>

    </form>
    <div>
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="${pageContext.request.contextPath
                    }/FindUserByPageServlet?currentPage=${pb.currentPage-1}&rows=5&name=${condition.name[0]}&${condition.address[0]}&${condition.email[0]}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <c:forEach begin="1" end="${pb.totalPage}" var="i">
                <li><a href="${pageContext.request.contextPath
                    }/FindUserByPageServlet?currentPage=${i}&rows=5&name=${condition.name[0]}&${condition.address[0]}&${condition.email[0]}">${i}</a></li>
                </c:forEach>
                <%--<li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>--%>
                <li>
                    <a href="${pageContext.request.contextPath
                    }/FindUserByPageServlet?currentPage=${pb.currentPage+1}&rows=5&name=${condition.name[0]}&${condition.address[0]}&${condition.email[0]}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
                <%--<span style="font-size: 25px;margin-left: 10px" >共16条记录,共4页</span>--%>
                <span style="font-size: 25px;margin-left: 10px" >共${pb.totalCount}条记录,共${pb.totalPage}页</span>
            </ul>

        </nav>
    </div>
</div>
</body>
</html>

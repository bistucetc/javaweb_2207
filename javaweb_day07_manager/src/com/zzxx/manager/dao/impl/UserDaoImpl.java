package com.zzxx.manager.dao.impl;

import com.zzxx.manager.domain.User;
import com.zzxx.manager.dao.UserDao;
import com.zzxx.manager.utils.DruidUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserDaoImpl implements UserDao {
    //连接数据库
    private JdbcTemplate template =
            new JdbcTemplate(DruidUtils.getDataSource());

    @Override//查询用户
    public User findUserByNameAndPwd(String name, String password) {
        //1.编写sql
        String sql = "select * from user where name = ? and password = ?";
        //2.执行sql
        List<User> list = template.query(sql,
                new BeanPropertyRowMapper<User>(User.class),
                name, password);
        /*
            获取的值:
            返回的是list集合中的第一个元素 -> get(0)
            不确定get(0) 有没有元素,所以要对集合进行判断
            list.size() == 0 返回 null
            list.size() != 0 返回list.get(0)

         */
        return list.size() == 0 ? null : list.get(0);
    }

    //查询全部
    public List<User> selectAll() {
        //定义sql
        String sql = "select * from user";
        List<User> list = template.query(sql,
                new BeanPropertyRowMapper<User>(User.class));
        return list;
    }

    @Override
    public void add(User user) {
        String sql = "insert into user values(null,?,?,?,?,?,?,?)";
        template.update(
                sql,user.getName(),user.getGender(),
                user.getAge(),user.getAddress(),
                user.getQq(),user.getEmail(),user.getPassword());
    }

    @Override
    public void delete(int id) {
        //1.定义sql
        String sql = "delete from user where id = ?";
        //2.执行sql
        template.update(sql,id);
    }

    @Override//根据id查询
    public User findById(int id) {
        String sql = "select * from user where id = ?";
        User user = template.queryForObject(sql,
                new BeanPropertyRowMapper<User>(User.class),
                id);
        return user;
    }

    @Override//修改用户信息方法
    public void update(User user) {
        String sql = "update user set name = ?" +
                ",gender = ?,age = ?,address = ?,qq = ?," +
                " email = ? where id = ?";
        template.update(sql,user.getName(),user.getGender(),
                user.getAge(),user.getAddress(),
                user.getQq(),user.getEmail(),user.getId());
    }

    @Override//查询总条数
    public int findTotalCount(Map<String, String[]> condition) {
        //1.定义sql模板
        String sql = "select count(*) from user where 1 = 1";
        //3.新建StringBuiler
        StringBuilder sb = new StringBuilder(sql);
        //4.新建ArrayList集合,用于存储条件的值
        ArrayList<Object> params = new ArrayList<>();
        //2.遍历map集合
        Set<String> keySet = condition.keySet();
        for (String key : keySet) {
            //跳过分页参数
            if("currentPage".equals(key) || "rows".equals(key)){
                continue;
            }
            //2.1根据key 获取 value值
            String value = condition.get(key)[0];
            System.out.println(key + ":" + value);
            //2.2判断value是否有值
            if(value != null && !"".equals(value)){
                //2.3有值,则做sql的拼接
                //sb.append(" and name like %张%");
                sb.append(" and "+key+" like ? ");
                //3.1 将 ? 后面的数据存储到集合中
                params.add("%"+ value +"%");
            }
        }
        System.out.println(sb.toString());
        System.out.println(params);
        return template.queryForObject(sb.toString(),
                Integer.class,params.toArray());
    }

    @Override//查询list集合
    public List<User> findByPage(int start, int row, Map<String, String[]> condition) {
        //分页查询
        String sql = "select * from user where 1 = 1";
        //3.新建StringBuiler
        StringBuilder sb = new StringBuilder(sql);
        //4.新建ArrayList集合,用于存储条件的值
        ArrayList<Object> params = new ArrayList<>();
        //2.遍历map集合
        Set<String> keySet = condition.keySet();
        for (String key : keySet) {
            if ("currentPage".equals(key) || "rows".equals(key)){
                continue;
            }
            //2.1根据key 获取 value值
            String value = condition.get(key)[0];
            //2.2判断value是否有值
            if (value != null && !"".equals(value)){
                //2.3有值,则做sql的拼接
                //sb.append(" and name like %张%");
                sb.append(" and " +key+ " like ?");
                //3.1 将 ? 后面的数据存储到集合中
                params.add("%"+ value +"%");
            }
        }
        sb.append(" limit ?,?");
        params.add(start);
        params.add(row);
        sql = sb.toString();
        return template.query(sql,
                new BeanPropertyRowMapper<User>(User.class),
                params.toArray());
    }
}






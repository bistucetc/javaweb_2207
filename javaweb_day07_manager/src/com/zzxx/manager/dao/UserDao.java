package com.zzxx.manager.dao;

import com.zzxx.manager.domain.User;

import java.util.List;
import java.util.Map;

public interface UserDao {
    //根据用户名和密码查询用户
    User findUserByNameAndPwd(String name, String password);

    //查询全部用户
    List<User> selectAll();

    //添加用户
    void add(User user);

    void delete(int id);

    User findById(int id);

    void update(User user);

    int findTotalCount(Map<String, String[]> condition);

    List<User> findByPage(int start, int row, Map<String, String[]> condition);

}

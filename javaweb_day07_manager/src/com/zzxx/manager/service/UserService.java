package com.zzxx.manager.service;

import com.zzxx.manager.domain.PageBean;
import com.zzxx.manager.domain.User;
import com.zzxx.manager.exception.LoginException;

import java.util.List;
import java.util.Map;

/* 对用户进行业务逻辑处理 */
public interface UserService {

    //登录方法
    User login(User user) throws LoginException;

    //查询所有用户信息
    List<User> findAll();

    //添加用户
    void addUser(User user);

    //删除用户
    void deleteUser(String id);

    //根据id查询用户
    User findUserById(String id);

    //修改用户
    void updateUser(User user);

    //删除选中用户
    void delSelectUser(String[] uids);

    //分页查询
    PageBean findUserByPage(String currentPage, String rows, Map<String, String[]> condition);
}

package com.zzxx.manager.service.impl;

import com.zzxx.manager.domain.PageBean;
import com.zzxx.manager.domain.User;
import com.zzxx.manager.dao.UserDao;
import com.zzxx.manager.dao.impl.UserDaoImpl;
import com.zzxx.manager.exception.LoginException;
import com.zzxx.manager.service.UserService;

import java.util.List;
import java.util.Map;


//实现类
public class UserServiceImpl implements UserService {
    private UserDao ud = new UserDaoImpl();

    //登录方法
    @Override
    public User login(User user) throws LoginException {
        //1.查询数据
        User loginUser =
                ud.findUserByNameAndPwd(user.getName(),user.getPassword());

        //2.查询结果对比:  返回user / null
        if(loginUser != null){
            return loginUser;
        }else {
            //抛出异常
            throw new LoginException("用户名/密码错误!!!");
        }
    }

    @Override //查询所有用户
    public List<User> findAll() {
        //调用dao中的查询方法
        List<User> list = ud.selectAll();
        return list;
    }

    @Override//添加用户
    public void addUser(User user) {
        //调用dao方法添加用户
        ud.add(user);
    }

    @Override//删除用户
    public void deleteUser(String id) {
        //调用dao中删除的方法
        ud.delete(Integer.parseInt(id));
    }

    @Override//根据id查询用户
    public User findUserById(String id) {
        //调用dao中根据id查询的方法
        User user = ud.findById(Integer.parseInt(id));
        return user;
    }

    @Override
    public void updateUser(User user) {
        //调用dao中修改方法
        ud.update(user);
    }

    @Override
    public void delSelectUser(String[] uids) {
        //3.判断uids是否为空
        if(uids != null && uids.length > 0){
            //1.遍历数组
            for(String id : uids){
                //2.调用dao中的删除方法
                ud.delete(Integer.parseInt(id));
            }
        }

    }


    @Override//查询分页数据
    public PageBean<User> findUserByPage(String currentPage, String rows, Map<String, String[]> condition) {
        //2.将String -> int 将currentPage和rows转换成int类型
        int current = Integer.parseInt(currentPage);
        int row = Integer.parseInt(rows);

        //7.新增参数: condition,用户动态传入的信息

        //6.如果当前页面数 < 1 ,则让当前页面是 == 1
        if(current <= 0){
            current = 1;
        }

        //1.新建PageBean对象 -> null
        PageBean<User> pb = new PageBean<>();
        //2.1 设置pb的参数
        pb.setCurrentPage(current);
        pb.setRows(row);
        //3.设置totalCount -> 调用dao中方法,查询总记录数
        //符合当前条件(condition)下的总记录数
        int totalCount = ud.findTotalCount(condition);
        pb.setTotalCount(totalCount);

        //4.设置List -> 调用dao中方法
        //4.1计算 start = (总记录数(current) - 1) * row
        int start = (current - 1) * row;
        List<User> list = ud.findByPage(start,row,condition);
        pb.setList(list);

        //5.设置totalPage -> 进行计算
        int totalPage = (totalCount % row) == 0 ?
                (totalCount/row) : (totalCount/row + 1);
        pb.setTotalPage(totalPage);

        return pb;
    }
}








package com.zzxx.manager.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/*
    完成登录验证的过滤器
 */
@WebFilter("/*")
public class LoginFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        //1.强制转换成Servlet中使用的HttpServletRequest
        HttpServletRequest req = (HttpServletRequest) request;
        //2.获取请求资源路径
        String uri = req.getRequestURI();
        System.out.println(uri);
        //3.判断是否包含和登录相关的资源(cs/js/验证码)
        if (uri.contains("/login.jsp")
            || uri.contains("/LoginServlet")
            || uri.contains("/css/")
            || uri.contains("/js/")
            || uri.contains("/fonts/")
            || uri.contains("/checkCode")){
            //放行以上所有资源
            chain.doFilter(request,response);
        }else {
            //4.不包含登录验证所需资源 - 判断登录状态
            //判断session域中是否存在有user用户
            Object user = req.getSession().getAttribute("user");
            if (user != null){
                //登录过了,放行
                chain.doFilter(request,response);
            }else {
                //没有登录,跳转到登录页面
                //提示错误信息
                req.setAttribute("login_msg","尚未登录,请登录!");
                req.getRequestDispatcher("/login.jsp")
                        .forward(req,response);
            }

        }
    }

    @Override
    public void destroy() {

    }
}



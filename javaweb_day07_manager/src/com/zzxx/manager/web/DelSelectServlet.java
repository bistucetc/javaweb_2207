package com.zzxx.manager.web;

import com.zzxx.manager.service.UserService;
import com.zzxx.manager.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
/*
    删除选中功能
 */
@WebServlet("/DelSelectServlet")
public class DelSelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取所有的id
        String[] uids = request.getParameterValues("uid");
        //2.调用service中删除方法
        UserService us = new UserServiceImpl();
        us.delSelectUser(uids);

        //3.跳转到UserListServlet
        response.sendRedirect
                (request.getContextPath() + "/UserListServlet");

    }
}










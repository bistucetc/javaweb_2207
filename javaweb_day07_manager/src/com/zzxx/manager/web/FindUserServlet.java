package com.zzxx.manager.web;

import com.zzxx.manager.domain.User;
import com.zzxx.manager.service.UserService;
import com.zzxx.manager.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    回显操作
 */
@WebServlet("/FindUserServlet")
public class FindUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取到id
        String id = request.getParameter("id");
        //2.调用service中查询方法
        UserService us = new UserServiceImpl();
        User user = us.findUserById(id);
        //3.将user存入request域中
        request.setAttribute("user",user);
        //4.转发到update.jsp显示
        request.getRequestDispatcher("/update.jsp")
                .forward(request,response);
    }
}
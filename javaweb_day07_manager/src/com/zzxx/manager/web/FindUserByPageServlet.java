package com.zzxx.manager.web;

import com.zzxx.manager.domain.PageBean;
import com.zzxx.manager.service.UserService;
import com.zzxx.manager.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

@WebServlet("/FindUserByPageServlet")
public class FindUserByPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        //1.获取参数 currentPage,rows
        //获取当前页面
        String currentPage = request.getParameter("currentPage");
        //获取每页显示的条数
        String rows = request.getParameter("rows");

        /*
            复杂条件查询: 获取条件参数
                condition中包含: 用户动态输入的信息(姓名,地址,email)
         */
        Map<String, String[]> condition = request.getParameterMap();

        System.out.println();
        //5.解决空指针问题 -> currentPage == 0
        if (currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if (rows == null || "".equals(rows)){
            rows = "5";
        }


        //2.调用service方法查询
        UserService us = new UserServiceImpl();
        PageBean pb = us.findUserByPage(currentPage,rows,condition);
        //测试并查看pb对象是否获取成功
        System.out.println(pb);

        //3.将pb对象存入到域对象中
        request.setAttribute("pb",pb);

        //4.转发到list.jsp
        request.getRequestDispatcher("/list.jsp")
                .forward(request,response);

    }
}









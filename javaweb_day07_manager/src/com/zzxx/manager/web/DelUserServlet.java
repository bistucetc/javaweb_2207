package com.zzxx.manager.web;

import com.zzxx.manager.service.UserService;
import com.zzxx.manager.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/DelUserServlet")
public class DelUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取参数 - id
        String id = request.getParameter("id");

        //2.调用删除方法
        UserService us = new UserServiceImpl();
        us.deleteUser(id);

        //3.页面跳转
        response.sendRedirect
                (request.getContextPath() + "/UserListServlet");
    }
}









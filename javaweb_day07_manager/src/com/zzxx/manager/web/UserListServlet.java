package com.zzxx.manager.web;

import com.zzxx.manager.domain.User;
import com.zzxx.manager.service.UserService;
import com.zzxx.manager.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

// 显示用户列表功能
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.调用service方法 获取用户列表
        UserService us = new UserServiceImpl();
        List<User> list = us.findAll();
        System.out.println(list);

        //2.跳转界面,将list集合中的用户显示在list.jsp
        request.setAttribute("list",list);
        //response.sendRedirect(request.getContextPath() + "/list.jsp");
        request.getRequestDispatcher("/list.jsp")
                .forward(request,response);
    }
}











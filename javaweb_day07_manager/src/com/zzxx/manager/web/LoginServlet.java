package com.zzxx.manager.web;

import com.zzxx.manager.domain.User;
import com.zzxx.manager.exception.LoginException;
import com.zzxx.manager.service.UserService;
import com.zzxx.manager.service.impl.UserServiceImpl;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

//登录业务
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //0.设置request字符集
        request.setCharacterEncoding("UTF-8");
        //1.接收请求参数
        Map<String, String[]> map = request.getParameterMap();
        //1.1封装对象
        User user = new User();
        try {
            BeanUtils.populate(user,map);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        //4.获取验证码 - 前端传回来
        String verifycode = request.getParameter("verifycode");
        //4.1 程序生成的验证码"CHECKCODE_SERVER" 和 传回来的验证码作对比
        HttpSession session = request.getSession();
        String checkcode_server = (String)session.getAttribute("CHECKCODE_SERVER");
        //删除使用过的session
        session.removeAttribute("CHECKCODE_SERVER");
        //校验时忽略大小写
        if (!checkcode_server.equalsIgnoreCase(verifycode)){
            //响应错误信息 -> 响应到login.jsp
            // 显示错误信息
            session.setAttribute("msg","验证码错误!!");
            //登录失败 - 请求转发 到 当前页面 login.jsp
            request.getRequestDispatcher
                    ("/login.jsp").forward(request,response);
            return;
        }

        //2.登录业务 -> UserService的login()
        UserService us = new UserServiceImpl();
        try {
            //查询结果: user / null
            User loginUser = us.login(user);
            //3.对结果判断,页面跳转

            //3.2 将用户信息存储到 域对象中
            session.setAttribute("user",loginUser);
            //3.1 登录成功 - 重定向跳转到 index.jsp
            response.sendRedirect
                    (request.getContextPath() + "/index.jsp");
        } catch (/*Exception*/LoginException e) {
            //3.4 显示错误信息
            request.setAttribute("msg",e.getMessage());
            //3.3 登录失败 - 请求转发 到 当前页面 login.jsp
            request.getRequestDispatcher
                    ("/login.jsp").forward(request,response);
        }
    }
}
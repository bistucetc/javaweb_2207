package com.zzxx.manager.domain;

import java.util.List;
import java.util.Objects;

/*
    分页对象
        添加泛型是为了使分页对象在不同情况(页面)可以进行使用
        List<T>就可以存储任意类型的数据
        例如: 商品分页,订单列表分页等....
 */
public class PageBean<T> {
    private Integer totalCount; //总记录数
    private Integer totalPage; //总页码
    private List<T> list; //每页中的数据
    private Integer currentPage; //当前页码
    private Integer rows; //每页显示的记录数

    public PageBean() {
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", list=" + list +
                ", currentPage=" + currentPage +
                ", rows=" + rows +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageBean<?> pageBean = (PageBean<?>) o;
        return Objects.equals(totalCount, pageBean.totalCount) && Objects.equals(totalPage, pageBean.totalPage) && Objects.equals(list, pageBean.list) && Objects.equals(currentPage, pageBean.currentPage) && Objects.equals(rows, pageBean.rows);
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalCount, totalPage, list, currentPage, rows);
    }
}

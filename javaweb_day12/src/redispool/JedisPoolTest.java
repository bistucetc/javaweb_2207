package redispool;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;


/*
    redis 的连接池
 */
public class JedisPoolTest {
    @Test
    public void test02(){
        //1.创建配置对象
        JedisPoolConfig config = new JedisPoolConfig();
        //设置最大空闲连接
        config.setMaxIdle(10);
        //设置允许的最大连接
        config.setMaxTotal(50);
        //创建连接池对象
        JedisPool pool = new JedisPool(config, "172.20.10.5",
                6379, Protocol.DEFAULT_TIMEOUT, "123456");

        //通过连接池对象获取连接
        Jedis jedis = pool.getResource();
        //执行操作
        String name = jedis.get("name");
        System.out.println(name);
    }
    @Test
    public void test01(){
        //1.创建配置对象
        Jedis jedis = JedisPoolUtils.getJedis();
        //2 设置密码授权
        jedis.auth("123456");
        String name = jedis.get("name");
        System.out.println(name);
    }
}

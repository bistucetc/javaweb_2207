package redis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

/*
    redis的连接
      ①.获取连接
      ②.操作
      ③.关闭连接
 */
public class RedisTest {
    @Test
    public void test01(){
        /*1.获取连接
            设置redis服务的所在的ip地址 和 redis的端口号
         */
        Jedis jedis = new Jedis("172.20.10.5",6379);
        System.out.println(jedis);
        //设置密码授权
        jedis.auth("123456");
        //2.获取数据
        String name = jedis.get("name");
        String set = jedis.set("sex", "nan");
        System.out.println(set);
        System.out.println(name);
        //3.关闭连接
        jedis.close();
    }

    @Test
    public void test02(){

        /*1.获取连接
            设置redis服务的所在的ip地址 和 redis的端口号
         */
        Jedis jedis = new Jedis("172.20.10.5",6379);
        System.out.println(jedis);
        //设置密码授权
        jedis.auth("123456");

        //存储hash
        jedis.hset("user","name","lisi");
        jedis.hset("user","age","23");
        jedis.hset("user","gender","f");

        //获取hash
        String hget = jedis.hget("user", "name");
        System.out.println(hget);

    }




}






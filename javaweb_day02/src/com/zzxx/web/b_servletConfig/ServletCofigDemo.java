package com.zzxx.web.b_servletConfig;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;
/*
    配置与使用
 */
@WebServlet(value = "/ServletCofigDemo",loadOnStartup = 1)
public class ServletCofigDemo extends HttpServlet {
    //创建ServletConfig对象
    private ServletConfig servletConfig;

    //在servlet对象创建时,就随之产生
    @Override
    public void init(ServletConfig config) throws ServletException {
        servletConfig = config;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println(servletConfig);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write(new Date().toString());
    }
}
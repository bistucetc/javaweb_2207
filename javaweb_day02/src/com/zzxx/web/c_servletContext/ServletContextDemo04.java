package com.zzxx.web.c_servletContext;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;

/*
    ServletContext作为域对象使用 - 用于取数据
 */
@WebServlet("/ServletContextDemo04")
public class ServletContextDemo04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = request.getServletContext();
        //取出域对象中到的数据 - 根据key获取value
        Date date = (Date)context.getAttribute("date");
        response.getWriter().write("date:" + date);
        String name = (String)context.getAttribute("name");
        response.getWriter().write("name:" + name);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
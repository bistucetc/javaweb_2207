package com.zzxx.web.c_servletContext;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;

/*
    ServletContext的常用方法 (作用)
 */
@WebServlet("/servletContextDemo02")
public class ServletContextDemo02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.创建对象
        ServletContext context = request.getServletContext();
        //2.获取应用初始化的参数
        String configPath = context.getInitParameter("configPath");
        System.out.println(configPath);

        //3.获取应用访问的虚拟目录(重要!!!)
        String contextPath = context.getContextPath();
        // /javaweb_day02_war_exploded
        System.out.println(contextPath);

        //4.根据虚拟目录获取应用中的绝对路径(重要!!!)
        //4.1 src 新建 a.txt   读取的是web应用中的路径: WEB-INF/classes/a.txt
        String aPath = context.getRealPath("WEB-INF/classes/a.txt");
        System.out.println(aPath);

        //4.2 web / WEB-INF 新建 b.txt 读取的是web应用中的路径: /WEB-INF/b.txt
        String bPath = context.getRealPath("/WEB-INF/b.txt");
        System.out.println(bPath);

        //4.3 web 新建 c.txt  读取的是web应用中的路径:c.txt
        String cPath = context.getRealPath("c.txt");
        System.out.println(cPath);

        //4.4 工程下 新建 d.txt  因为文件不会进行编译


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write(new Date().toString());
    }
}
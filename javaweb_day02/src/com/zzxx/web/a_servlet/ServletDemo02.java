package com.zzxx.web.a_servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;

public class ServletDemo02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取请求路径
        String uri = request.getRequestURI();
        //获取的路径: /javaweb02/servlet02
        //2.对uri路径进行截取
        uri = uri.substring(uri.lastIndexOf("/"), uri.length());
        System.out.println(uri);
        //2.判断当前路径(uri) 到底是那种请求方式
        if (uri.equals("/servlet02")){
            System.out.println("访问方式一");
        }else if (uri.equals("/web02")){
            System.out.println("访问方式二");
        }else if (uri.equals("/he")){
            System.out.println("访问方式三");
        }
        response.getWriter().write(new Date().toString());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write(new Date().toString());
    }
}
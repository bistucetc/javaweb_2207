package b_register;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
/*
    regist.html 跳转的 Servlet资源
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.设置编码集
        request.setCharacterEncoding("UTF-8");
        //2.获取请求参数
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String realname = request.getParameter("realname");
        String gender = request.getParameter("gender");
        String birthday = request.getParameter("birthday");
        //3.处理请求参数
        //3.1连接数据库 - druid连接池
        JdbcTemplate template =
                new JdbcTemplate(DruidUtils.getDataSource());
        //3.2编写sql
        String sql = "insert into user values(null,?,?,?,?,?,?)";
        //给 ? 进行赋值操作
        //3.3执行sql
        template.update(sql,username,password,
                email,realname,gender,birthday);

    }
}
package a_request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;

/*
    获取请求行信息 :请求方式 + URI + 协议版本
 */
@WebServlet("/ServletRequestDemo01")
public class ServletRequestDemo01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取请求方式
        String method = request.getMethod();
        System.out.println("请求方式:" + method);
        //2.获取URI -> 虚拟目录 + 具体资路径
        String uri = request.getRequestURI();
        System.out.println("uri:" + uri);
        StringBuffer url1 = request.getRequestURL();
        String url = url1.toString();
        System.out.println("url:" + url);
        //4.获取虚拟目录(重要!!!)
        String path = request.getContextPath();
        System.out.println("path" + path);
        //5.获取Servlet路径
        String servletPath = request.getServletPath();
        System.out.println("servletPath" + servletPath);

        //6.获取请求参数 - 只适用于get的提交方式
        String queryString = request.getQueryString();
        System.out.println("请求参数:" + queryString );


        //3.获取协议版本
        String protocol = request.getProtocol();
        System.out.println(protocol);
    }
}
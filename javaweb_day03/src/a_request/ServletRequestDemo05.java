package a_request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/*
    解决乱码问题
 */
@WebServlet("/ServletRequestDemo05")
public class ServletRequestDemo05 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*
            所有的servlet资源默认都是get的提交方式

            get提交方式: 编码集与tomcat的一致
                也就意味着,修改tomcat编码集,会改变get的编码集
            tomcat原始编码集: ISO8859-1
         */

        //1.接收请求参数
        String username = request.getParameter("username");
        System.out.println("username:" + username);
        //2.解决乱码
        username = new String(username.getBytes
                ("ISO8859-1"), "utf-8");
        System.out.println("username:" + username);
    }

    /* 解决post请求参数乱码问题:
    *
    *   只要是post提交,就一定会出现乱码,出现乱码的地方
    *   在Tomcat 原来编码: ISO8859-1
    *   request -> 获取
    * */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.接收请求参数之前,需要先解决乱码
        request.setCharacterEncoding("UTF-8");
        //2.接收请求参数
        String username = request.getParameter("username");
        System.out.println("username:" + username);
    }
}
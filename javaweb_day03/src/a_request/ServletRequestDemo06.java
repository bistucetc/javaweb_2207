package a_request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/*
不同方式获取请求参数
    根据参数名称获取参数值:String getParameter(String name)
    根据参数名称获取参数值的数组: String[] getParameterValues(String name)
    获取所有请求的参数名称:Enumeration<String>  getParameterNames()
    获取所有参数的map集合:Map<String,String[]> getParameterMap()
 */
@WebServlet("/ServletRequestDemo06")
public class ServletRequestDemo06 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        //1.获取的参数 一个name 对应一个 value
        String username = request.getParameter("username");
        System.out.println("username" + username);

        System.out.println("-----------------");

        //2.获取的参数 一个name对应 多个value值
        String[] hobbies = request.getParameterValues("hobby");
        System.out.println(Arrays.toString(hobbies));

        System.out.println("-----------------");

        //3.获取所有参数,将参数封装成map集合,ParameterMap
        Map<String, String[]> map = request.getParameterMap();
        map.entrySet().forEach(entrySet -> {
            System.out.println(entrySet.getKey() + ":"+ Arrays.toString(entrySet.getValue()));
        });
    }
}
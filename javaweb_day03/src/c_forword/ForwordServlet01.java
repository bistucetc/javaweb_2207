package c_forword;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/ForwordServlet01")
public class ForwordServlet01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取虚拟目录
        //String path = request.getServletContext().getContextPath();
        String path = request.getContextPath();
        System.out.println(path);


        //2.request作为域对象使用
        request.setAttribute("name","lucy");
        //测试request请求是否同一个
        System.out.println("Forword01: " + request);
        System.out.println("要跳转到Forword02中.....");

        //3.做请求转发功能, 在ForwordServlet02中取出域对象的数据
        /*
        //3.1获取请求的转发器 - 参数:要转发到那个servlet资源
        RequestDispatcher rd = request.getRequestDispatcher("/ForwordServlet02");
        //3.2请求转发
        rd.forward(request,response);

         */
        //合并以上操作
        request.getRequestDispatcher
                ("/ForwordServlet02").forward(request,response);

        //转发后的代码.就不在进行执行操作!!!!
    }
}
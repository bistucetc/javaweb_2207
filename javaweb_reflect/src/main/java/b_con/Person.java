package b_con;

/*
    实体类
 */
public class Person {
    private String name;
    private int age;

    //私有的构造方法
    private Person(String name) {
        System.out.println("name:" + name);
        System.out.println("age:" + age);
    }
    private Person(String name,int age) {
        System.out.println("name:" + name);
        System.out.println("age:" + age);
    }

    //公开的构造方法
    public Person() {
        System.out.println(name +":" + age);
    }
}

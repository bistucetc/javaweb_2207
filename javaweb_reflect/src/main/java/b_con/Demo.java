package b_con;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
Constructor<?>[] getConstructors()
Constructor<?>[] getDeclaredConstructors()

Constructor<T> getConstructor(类<?>... parameterTypes)
Constructor<T> getDeclaredConstructor(类<?>... parameterTypes)

 */
public class Demo {
    @Test
    public void test01() throws ClassNotFoundException {
        //1获取类的字节码对象
        Class c = Class.forName("b_con.Person");

        //获取当前类中所有公共的构造方法
        Constructor[] cs = c.getConstructors();
        for (Constructor constructor:cs) {
            System.out.println(constructor);
        }
    }

    @Test
    public void test02() throws ClassNotFoundException {
        //1获取类的字节码对象
        Class c = Class.forName("b_con.Person");

        Constructor[] cs = c.getDeclaredConstructors();
        for (Constructor constructor:cs) {
            System.out.println(constructor);
        }
    }

    @Test//创建对象
    public void test03() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //1获取类的字节码对象
        Class c = Class.forName("b_con.Person");

        Constructor constructor = c.getDeclaredConstructor(String.class,int.class);
        System.out.println(constructor);

        //设置暴力反射
        constructor.setAccessible(true);

        //创建对象的方式
        Person per  = (Person)constructor.newInstance("lucy", 20);
        System.out.println(per);
    }





}

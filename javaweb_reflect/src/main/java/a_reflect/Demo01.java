package a_reflect;

import com.zzxx.domain.Student;

/*
    获取Class对象的方式
 */
public class Demo01 {
    public static void main(String[] args) throws ClassNotFoundException {
        //1.Class.forName("全限定类名")
        Class c1 = Class.forName("com.zzxx.domain.Student");

        //2.类名.class
        Class c2 = Student.class;

        //3.对象名.getClass
        Student stu = new Student();
        Class c3 = stu.getClass();

        System.out.println(c1 == c2);
        System.out.println(c3 == c2);

    }
}








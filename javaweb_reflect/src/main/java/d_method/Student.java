package d_method;

public class Student {
    //私有的 无参无返回值
    private void show(){
        System.out.println("私有的show()");
    }

    //公有的,无参无返回值
    public void method01(){
        System.out.println("method01");
    }

    //公有的,有参无返回值
    public void method02(int age){
        System.out.println("method02");
    }

    //公有的,无参有返回值
    public String method03(){
        System.out.println("method01");
        return "你好!!";
    }

    //公有的,有参有返回值
    public String method04(String name){
        System.out.println("method01");
        return "hello!!";
    }
}

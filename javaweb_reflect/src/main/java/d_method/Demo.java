package d_method;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Demo {
    @Test
    public void test01() throws ClassNotFoundException, NoSuchMethodException {
        //1.获取Student的字节码对象
        Class c = Class.forName("d_method.Student");

        //获取所有方法(公开的,私有的)
        Method[] methods = c.getDeclaredMethods();
        for (Method method: methods) {
            System.out.println(method);
        }

        System.out.println("---------------");

        //获取单个方法
        Method method01 = c.getMethod("method01");
        System.out.println(method01);
        Method method02 = c.getMethod("method02", int.class);
        System.out.println(method02);
    }

    @Test //执行方法
    public void test02() throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        /*
            Object invoke(Object obj, Object... args)
                参数1: 调用谁(那个对象)的方法
                参数2: 调用方法中的传递的参数值(如果没有就不写)
                返回值: Object, 当前调用方法的返回值(如果没有就不写)
         */

        //1.获取Student的字节码对象
        Class c = Class.forName("d_method.Student");

        //2.获取当前要调用的方法
        Method method = c.getMethod("method04", String.class);

        //method.setAccessible(true);
        //新建对象
        Student stu = (Student) c.newInstance();

        //3.调用运行方法
        Object invoke = method.invoke(stu,"tom");
        System.out.println(invoke);
    }
}










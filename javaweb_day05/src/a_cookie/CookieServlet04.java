package a_cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    问题2:Cookie在浏览器的保存时间为多久?
 */
@WebServlet("/CookieServlet04")
public class CookieServlet04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.创建Cookie对象
        Cookie c1 = new Cookie("msg", "你好");

        //2.设置cookie的默认存活时间
        //让cookie存活60秒
        c1.setMaxAge(30);

        //表示浏览器关闭,cookie消失
        //c1.setMaxAge(-1);

        //3.发送cookie
        response.addCookie(c1);

    }
}
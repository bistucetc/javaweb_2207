package a_cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    接收Cookie
 */
@WebServlet("/CookieServlet05")
public class CookieServlet05 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取cookie
        Cookie[] cs = request.getCookies();
        if (cs != null){
            String name = cs[0].getName();
            String value = cs[0].getValue();
            response.getWriter()
                    .write("success:" + name+ " , " + value);
        }
    }
}
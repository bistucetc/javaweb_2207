package a_cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;


/*
    获取Cookie
  	使用步骤:
  	  ①.获取客户端携带的所有的Cookie,使用request对象进行获取
  	  	Cookie[] cs = request.getCookies();
  	  ②.遍历数组,获取每一个Cookie对象
  	  ③.使用cookie对象方法获取数据
  	  	cs.getName()
  	  	cs.getValue()
 */
@WebServlet("/CookieServlet02")
public class CookieServlet02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.接收到请求
        System.out.println("接收到请求");

        //2.通过请求对象获取cookie
        //注意: cs有可能为null
        Cookie[] cs = request.getCookies();
        if (cs != null){
            String name = cs[0].getName();
            String value = cs[0].getValue();
            response.getWriter()
                    .write("success:" + name+ " , " + value);

        }else {
            response.getWriter().write("Cookie is null!!");
        }


    }
}
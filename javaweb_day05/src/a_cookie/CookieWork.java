package a_cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
    案例: 记住上一次访问时间
    需求：
	1.访问一个Servlet，如果是第一次访问，则提示：您好，欢迎您首次访问。
	 2.如果不是第一次访问，则提示：欢迎回来，您上次访问时间为:显示时间字符串
 */
@WebServlet("/CookieWork")
public class CookieWork extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //5.设置响应编码
        response.setContentType("text/html;charset=utf-8");

        //1.获取所有的Cookie,判断 是否有名为lastTime的值
        Cookie[] cookies = request.getCookies();
        //6.1 有cookie,但是没有名为 lastTime的cookie
        boolean flag = false;

        //2.遍历Cookie数组
        if (cookies != null && cookies.length > 0){
            //获取到每一个cookie
            for (Cookie cookie : cookies) {
                //3.获取cookie名称
                String name = cookie.getName();

                //4.判断名称是否为: lastTime
                if ("lastTime".equals(name)){
                    //4.1 如果有cookie,证明不是第一次访问
                    //6.2如果找到lastTime的cookie,就标记为true
                    flag = true;

                    //4.2 响应数据
                    //4.2-1 获取lastTime所对应的value,时间
                    String value = cookie.getValue();
                    //4.2-2 输出字符串
                    response.getWriter().write("访问时间: " + value);

                    //4.2-3设置当前时间-> new Date() -> String -> 拼接
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //Date -> String
                    String now = sdf.format(date);

                    //7.处理 不支持特殊字符的问题: 例如: 32-> 空格
                    System.out.println("编码前: " + now);
                    now = URLEncoder.encode(now, "utf-8");
                    System.out.println("编码后: " + now);

                    //设置Cookie时间
                    cookie.setValue(now);
                    //发送cookie
                    response.addCookie(cookie);

                    //4.3设置持久化存储时间
                    cookie.setMaxAge(60 * 60 * 24 * 30);//一个月
                }
            }
        }
        //6.没有lastTime的情况,第一次访问
        if (cookies == null || cookies.length == 0 || flag == false){
            //6.3 设置cookie
            //获取当前系统时间重置cookie
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //Date -> String
            String now = sdf.format(date);

            //7.处理 不支持特殊字符的问题: 例如: 32-> 空格
            now = URLEncoder.encode(now, "utf-8");

            //6.4 重置cookie的值
            Cookie cookie = new Cookie("lastTime", now);
            //6.5 设置cookie的持久化时间
            cookie.setMaxAge(60*60*24*30);
            //6.6发送cookie
            response.addCookie(cookie);
            //6.7 输出信息
            response.getWriter().write("欢迎首次访问!!");
        }

    }
}














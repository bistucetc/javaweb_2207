package a_cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
/*
    发送Cookie
  	使用步骤:
  	  ①.创建Cookie对象,设置要发送的数据
  	  	Cookie c = new Cookie("key","value")
  	  ②.发送cookie到客户端中,使用response对象发送
  	  	response.addCookie(c);
 */
@WebServlet("/CookieServlet01")
public class CookieServlet01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.接收请求
        System.out.println("接收到请求");
        //2.做一系列的的业务处理
        System.out.println("做一系列的的业务处理");

        //3.将数据存储到cookie中,通过response对象携带到客户端中
        //3①.创建Cookie对象,设置要发送的数据
        //cookie仅支持String数据格式,只能使用英文
        Cookie c = new Cookie("name","zhangsan");
        //3②.发送cookie到客户端中,使用response对象发送
        response.addCookie(c);
        //等同于 = response.setHeader("Set-Cookie","cookie的值");

        response.getWriter().write("success!!");


    }
}









package a_cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    问题3:Cookie共享问题,共享的范围多大?
      默认情况下: cookie是不能共享
 */
@WebServlet("/CookieServlet06")
public class CookieServlet06 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.创建cookie对象
        Cookie c = new Cookie("msg", "hello");
        //3.设置path,共享路径 -> 让当前服务器下部署的所有web应用都共享cookie
        c.setPath("/");
        //设置单独服务器能共享 - 提供虚拟目录
        //c.setPath("/javaweb_day04");
        //2发送cookie
        response.addCookie(c);
    }
}






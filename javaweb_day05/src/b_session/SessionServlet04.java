package b_session;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/*
    session的销毁
 */
@WebServlet("/SessionServlet04")
public class SessionServlet04 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取session域中的数据

        //1.获取session对象
        HttpSession session = request.getSession();
        System.out.println(session);
        //3.手动销毁session对象
        session.invalidate();
        //2.获取session域中的数据
        Object msg = session.getAttribute("msg");
        System.out.println(msg);
    }
}
面试题默写：
1.什么是static?有什么特点?
2.String和StringBuilder、StringBuffer的区别?
3.重载（Overload）和重写（Override）的区别
4.抽象类（abstract class）和接口（interface）的区别

_________________________________________
复习:
1.请求(request): 请求头,请求行,请求体
  域对象范围: 一次请求
  请求转发

2.响应(response): 响应头,响应行,响应体
  重定向(302)

____________________________________________
一、会话技术

1.回顾http协议: 基于请求与响应模式的无状态协议.
  无状态:
  	协议对于所有的事务处理都没有记忆能力,如果不经过设置,服务器时不自动维护用户保存信息的功能,所以无法保存用户状态.

2.会话技术概述
  1)从打开你浏览器访问某个网站,到关闭浏览器的过程,称之为一次会话.

  2)会话技术就是指在会中,帮助服务器记录用户状态和数据的技术.

  3)作用:在不同请求间实现数据的共享.

3.会话技术的分类
  1)Cookie: 客户端的会话技术
  	特点:把要共享的数据保存到客户端(浏览器),每次请求时,把会话信息带到服务器中,从而实现多次请求间共享数据.

  2)Session: 服务器端的会话技术
  	特点:它本质上还是采用了Cookie技术进行存储,只不过保存到客户端是使用一个特殊标记的形式.
  	并且把要共享额数据保存到了服务器的内存对象中.

----------------------------------------------
二、Cookie: 客户端的会话技术
1.概述
  Cookie是浏览器中缓存文件，里面记录了客户端浏览器访问网站的一些相关信息.

2.Cookie的基本使用
  1)发送Cookie
  	使用步骤:
  	  ①.创建Cookie对象,设置要发送的数据
  	  	Cookie c = new Cookie("key","value")
  	  ②.发送cookie到客户端中,使用response对象发送
  	  	response.addCookie(c);
  2)获取Cookie
  	使用步骤:
  	  ①.获取客户端携带的所有的Cookie,使用request对象进行获取
  	  	Cookie[] cs = request.getCookies();
  	  ②.遍历数组,获取每一个Cookie对象
  	  ③.使用cookie对象方法获取数据
  	  	cs.getName()
  	  	cs.getValue()

2.Cookie的深入理解
  1)一次可不可以发送多个cookie?
  	可以创建并发送多个Cookie对象，使用response调用,多次addCookie方法发送cookie即可。

  2)cookie在浏览器中保存多长时间？
	默认情况下，当浏览器关闭后，Cookie数据被销毁
	持久化存储：setMaxAge(int seconds)
  	  - 正数：将Cookie数据写到硬盘的文件中,持久化存储。并指定cookie存活时间,时间到后,cookie文件自动失效。
	    - 负数：默认值
  	  - 零：删除cookie信息

  3)cookie共享问题,共享范围有多大？
  	默认情况下cookie不能共享.
	设置cookie的获取范围:setPath(String path)
	  默认情况下，设置当前的虚拟目录.
	  如果要共享，则可以将path设置为`/`


-----------------------------------------------
三、Session: 服务器端的会话技术
1.概述
  浏览器访问Web服务器的资源时,服务器会给每个用户浏览器创建一个Session对象,每个浏览器都独占一个Session对象.

  由于数据存储在客户端浏览器中容易被窃取或删除,所以存在很多不安全的因素,使用Session进行存储数据,则可以解决安全性问题.

  结论: Session的实现依赖于Cookie的.

2.Session的基本使用
  1)作为域对象使用
  	存储:setAttribute()
  	获取:getAttribute()
  	删除:removeAttribute()

3.Session的深入理解
  1)当客户端关闭后,服务器端不关闭,问两次获取的session是同一个吗?

  2)当客户端不关闭,服务器关闭后,两次获取的session是同一个吗?
    ①.所有的session对象都是存储在服务器的内存中,服务器重后,内存的数据应该已经被释放了,所以对应也会随之消失.

    session的钝化与活化:
      钝化:在服务器正常关闭后,Tomcat会自动将session数据写入到硬盘文件中(序列化的过程)

      活化:当再次启动服务器后,从文件加载数据到session中(反序列化的过程)

      注意:只有实现了序列化接口的类才能被序列化.

  3)session的生命周期
    ①.创建: 客户端第一次调用getSession()

    ②.销毁(销毁的方式)
      a.服务器非正常关闭 或者 应用被卸载.

      b.session过期
        默认情况下,无操作,30分钟后自动销毁
        手动设置过期时间:
          方式一: 在本工程下的web.xml,改变当前应用     
          <session-config>
              <session-timeout>10</session-timeout>
          </session-config>

          方式二:tomcat/conf/web.xml,改变所有web应用
            <session-config>
              <session-timeout>10</session-timeout>
          </session-config>

      c.手动销毁,调用session.invalidate()


4.Cookie和Session的区别
  1)相同点:Cookie 和 Session都是完成一次会话内多次请求间的数据共享.

  2)不同点:
    ①.存储位置:
      cookie是将数据存储在客户端
      session是将数据存储在服务器
    ②.安全性
      cookie不安全
      session安全
    ③.数据大小
      cookie存储有限制,最大3-4kb
      session无大小限制
    ④.存储时间
      cookie可以通过setMaxAge()设置长久存储
      session默认30分钟
    ⑤.服务器性能
      cookie不占用服务器内存资源
      session占用服务器内存资源

  3)应用场景
    cookie:购物车功能,10天免登陆功能
    session:登录功能中用户名展示,验证码功能

  4)结论:
    cookie:用来保证用户在不登录的情况下的身份识别
    session:用来保存用户登录后的数据



面试题:
1.Cookie和Session的区别?










